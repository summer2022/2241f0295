
# PWN Lab 官方文档
该项目本项目准备开发的实验室主要是针对 PWN 方向的 ctfer 初学者设计的，具有的可以快速部署题目，便捷切换 glibc 版本，拥有完善的调试工具，多种模板 exp 脚本和 shellcode 等特点，省去了前期大量花费在环境部署和调试上的时间，可以直接开始对 PWN 方向进行学习。项目基于 docker 开发，只需要简单的一行代码，就可以自动在本机上部署一个精简实用的 PWN 环境。

## 产品安装
1. 安装 Cloud Lab（https://gitee.com/tinylab/cloud-lab）   
    本产品基于 Cloud Lab 开发，是其一个子仓库，所以必须先安装 Cloud Lab ，及其所必须的 Docker Git 等。
    ```
    $ git clone https://gitee.com/tinylab/cloud-lab.git
    ```
2. 安装 pwn-lab  
    选择 pwn-lab 分支，通过 Cloud Lab 下的 tools/docker/choose 命令可以选择 pwn-lab 实验室即可安装。
    ```
    $ cd cloud-lab/
    $ git checkout pwn-lab
    $ tools/docker/choose pwn-lab
    ```

 
## 创建 pwn-lab
1. build 容器
```
$ tools/docker/build pwn-lab
```
2. 运行 pwn-lab 实验室（以 vnc 模式登陆）
```
$ LOGIN=vnc tools/docker/run pwn-lab
```
其他的登陆方式如：ssh webvnc bash
3. 停止 pwn-lab 实验室
```
$ tools/docker/stop pwn-lab
```
4. 重新运行 pwn-lab 实验室
```
$ tools/docker/start pwn-lab
```
5. 重新创建实验室
```
$ tools/docker/rerun pwn-lab
```
## pwn-lab 环境以及用法
### 环境  
pwn-lab 为ubuntu20.04的环境，所有相关文件均存放于 /labs/pwn-lab  目录下内容如下
1. tools  
    - ctf-xinetd
    - glibc-all-in-one
    - one_gadget
    - patchelf
    - peda
    - pwndbg
    - Pwngdb
    - ROPgadget
    - Ropper

2. scripts
    - clibc 
    - install
    - DownloadTools.sh
    - switch.sh

3. challenge  
    - 0x10.zip
### 用法
1. install  
用于初始用户安装全部 pwn 环境所需的插件，**需要在进入后第一时间运行**
```
$ cd /labs/pwn-lab/tools/scripts/
$ ./install
```
2. DownloadTools.sh  
用于下载所需要的软件（默认已经安装完毕，如果误删可运行此脚本）
```
$ cd /labs/pwn-lab/tools/scripts/
$ ./DownloadTools.sh
```
3. switch.sh  
用于切换自动化切换 pwngdb 插件，pwngdb 是 pwn 选手最常用的工具，但是作不同题目需要频繁切换插件，这个脚本可以帮助简化流程
```
$ cd /labs/pwn-lab/tools/scripts/
$ ./switch.sh 
Please choose one mode of GDB?
1.peda    2.gef    3.pwndbg
Input your choice:3
Please enjoy the pwndbg!
```
4. chlibc.sh  
用于自动切换libc版本的脚本，减少繁琐的命令输入
```
$ cd /labs/pwn-lab/tools/scripts/
$ sudo ./chlibc [libc_path] [filename]
```

5. 0x10.zip  
这是用于 pwn 选手练习的部分题目，存放于 /labs/pwn-lab/challenge/ 目录下
```
$ cd /labs/pwn-lab/challenge
$ unzip 0x10.zip
```  
解压后里面包含了题目和 exp ，其中 exp 仅用于提供模板和解题思路。  
