 cd /labs/pwn-lab/tools
 git clone https://github.com/pwndbg/pwndbg.git \
 && git clone https://github.com/longld/peda.git \
 && git clone https://github.com/NixOS/patchelf.git \
 && git clone https://github.com/scwuaptx/Pwngdb.git \
 && git clone https://github.com/matrix1001/glibc-all-in-one.git \
 && git clone https://github.com/david942j/one_gadget.git \
 && git clone https://github.com/Eadom/ctf_xinetd.git \
 && git clone https://github.com/lieanu/LibcSearcher.git\
 && git clone https://github.com/sashs/Ropper.git\
 && git clone https://github.com/JonathanSalwan/ROPgadget.git
